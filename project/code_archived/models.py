#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 15:56:05 2019
This code is a model library for GNN Atharva
@author: Shengli Jiang
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import dgl
import dgl.function as fn

##################Graph Convolutional Neural Network (spectral)##############
# Sends a message of node feature h.
msg = fn.copy_src(src='h', out='m')

def reduce(nodes):
    """Take an average over all neighbor node features hu and use it to
    overwrite the original node feature."""
    accum = torch.mean(nodes.mailbox['m'], 1)
    return {'h': accum}

class NodeApplyModule(nn.Module):
    """Update the node feature hv with ReLU(Whv+b)."""
    def __init__(self, in_feats, out_feats, activation):
        super(NodeApplyModule, self).__init__()
        self.linear = nn.Linear(in_feats, out_feats)
        self.activation = activation

    def forward(self, node):
        h = self.linear(node.data['h'])
        h = self.activation(h)
        return {'h' : h}
    
class GCN(nn.Module):
    def __init__(self, in_feats, out_feats, activation):
        super(GCN, self).__init__()
        self.apply_mod = NodeApplyModule(in_feats, out_feats, activation)

    def forward(self, g, feature):
        # Initialize the node features with h.
        g.ndata['h'] = feature
        g.update_all(msg, reduce)
        g.apply_nodes(func=self.apply_mod)
        return g.ndata.pop('h')
    
class SkNet(nn.Module):
    def __init__(self, in_dim, hidden_dim):
        super(SkNet, self).__init__()

        self.layers = nn.ModuleList([
            GCN(in_dim, hidden_dim, F.relu),
            GCN(hidden_dim, hidden_dim, F.relu),
            GCN(hidden_dim, hidden_dim, F.relu)])
        self.classify1 = nn.Linear(hidden_dim, hidden_dim, F.relu)
        self.classify2 = nn.Linear(hidden_dim, 8)

    def forward(self, g):
        # For undirected graphs, in_degree is the same as
        # out_degree.
        # the 'loc' data has (x,y,z)
        # the 'type' data is in R9, because 15 joints can be categorized into
        # 9 classes
        h_degree = g.in_degrees().view(-1, 1).float()
        h_loc = g.ndata['loc'].type(torch.FloatTensor)
        h_type = g.ndata['type'].type(torch.FloatTensor)
        h = torch.cat((h_degree, h_loc, h_type), axis=-1)#.cuda()
        for conv in self.layers:
            h = conv(g, h)
        g.ndata['h'] = h
        hg = dgl.mean_nodes(g, 'h')
        hg = self.classify1(hg)
        hg = self.classify2(hg)
        return hg



##################Graph Attention Neural Network (spatial)###################
class GATLayer(nn.Module):
    def __init__(self, in_dim, out_dim):
        super(GATLayer, self).__init__()
        # equation (1)
        self.fc = nn.Linear(in_dim, out_dim, bias=False)
        # equation (2)
        self.attn_fc = nn.Linear(2 * out_dim, 1, bias=False)

    def edge_attention(self, edges):
        # edge UDF for equation (2)
        z2 = torch.cat([edges.src['z'], edges.dst['z']], dim=1)
        a = self.attn_fc(z2)
        return {'e': F.leaky_relu(a)}

    def message_func(self, edges):
        # message UDF for equation (3) & (4)
        return {'z': edges.src['z'], 'e': edges.data['e']}

    def reduce_func(self, nodes):
        # reduce UDF for equation (3) & (4)
        # equation (3)
        alpha = F.softmax(nodes.mailbox['e'], dim=1)
        # equation (4)
        h = torch.sum(alpha * nodes.mailbox['z'], dim=1)
        return {'h': h}

    def forward(self, g, h):
        self.g = g
#        h_degree = g.in_degrees().view(-1, 1).float()
#        h_loc = g.ndata['loc'].type(torch.FloatTensor)
#        h_type = g.ndata['type'].type(torch.FloatTensor)
#        h = torch.cat((h_degree, h_loc, h_type), axis=-1)
        # equation (1)
        z = self.fc(h)
        self.g.ndata['z'] = z
        # equation (2)
        self.g.apply_edges(self.edge_attention)
        # equation (3) & (4)
        self.g.update_all(self.message_func, self.reduce_func)
        return self.g.ndata.pop('h')
    
class MultiHeadGATLayer(nn.Module):
    def __init__(self, in_dim, out_dim, num_heads, merge='cat'):
        super(MultiHeadGATLayer, self).__init__()
        self.heads = nn.ModuleList()
        for i in range(num_heads):
            self.heads.append(GATLayer(in_dim, out_dim))
        self.merge = merge

    def forward(self, g, h):
        head_outs = [attn_head(g, h) for attn_head in self.heads]
        if self.merge == 'cat':
            # concat on the output feature dimension (dim=1)
            return torch.cat(head_outs, dim=1)
        else:
            # merge using average
            return torch.mean(torch.stack(head_outs))
        
class GAT(nn.Module):
    def __init__(self, in_dim, hidden_dim, out_dim, num_heads):
        super(GAT, self).__init__()
        self.layer1 = MultiHeadGATLayer(in_dim, hidden_dim, num_heads)
        # Be aware that the input dimension is hidden_dim*num_heads since
        # multiple head outputs are concatenated together. Also, only
        # one attention head in the output layer.
        self.layer2 = MultiHeadGATLayer(hidden_dim * num_heads, out_dim, 1)

    def forward(self, g):
        # For undirected graphs, in_degree is the same as
        # out_degree.
        # the 'loc' data has (x,y,z)
        # the 'type' data is in R9, because 15 joints can be categorized into
        # 9 classes
        h_degree = g.in_degrees().view(-1, 1).float()
        h_loc = g.ndata['loc'].type(torch.FloatTensor)
        h_type = g.ndata['type'].type(torch.FloatTensor)
        h = torch.cat((h_degree, h_loc, h_type), axis=-1).cuda()
        h = self.layer1(g, h)
        h = F.elu(h)
        h = self.layer2(g, h)
        g.ndata['h'] = h
        h = dgl.mean_nodes(g, 'h')
#        h2 = []
#        for i in range(int(h_degree.size(0)/30)):
#            hg = h[i*30:(i+1)*30, :].mean(axis=0).unsqueeze(0)
#            h2.append(hg)
#        h2 = torch.cat(h2, axis=0)
        return h

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class GAT_LSTM(nn.Module):
    def __init__(self, in_dim, hidden_dim, out_dim, num_heads, lstm_units, lstm_layers, batch_size=3):
        super(GAT_LSTM, self).__init__()
        self.lstm_layers = lstm_layers
        self.batch_size = batch_size
        self.lstm_units = lstm_units
        self.layer1 = MultiHeadGATLayer(in_dim, hidden_dim, num_heads)
        # Be aware that the input dimension is hidden_dim*num_heads since
        # multiple head outputs are concatenated together. Also, only
        # one attention head in the output layer.
        self.layer2 = MultiHeadGATLayer(hidden_dim * num_heads, out_dim, 1)
        self.lstm = nn.LSTM(input_size=out_dim, 
                            hidden_size=lstm_units,
                            num_layers=lstm_layers,
                            batch_first=True)
        self.hidden_to_tag = nn.Linear(lstm_units, 8, F.relu)

    def init_hidden(self):
        # the weights are of the form (nb_layers, batch_size, nb_lstm_units)
        hidden_a = torch.randn(self.lstm_layers, self.batch_size, self.lstm_units)
        hidden_b = torch.randn(self.lstm_layers, self.batch_size, self.lstm_units)

        hidden_a = hidden_a.cuda()
        hidden_b = hidden_b.cuda()

        hidden_a = Variable(hidden_a)
        hidden_b = Variable(hidden_b)

        return (hidden_a, hidden_b)

    def forward(self, gs):
        # For undirected graphs, in_degree is the same as
        # out_degree.
        # the 'loc' data has (x,y,z)
        # the 'type' data is in R9, because 15 joints can be categorized into
        # 9 classes
        self.hidden = self.init_hidden()
        h_list = []
        for g in gs:
            h_degree = g.in_degrees().view(-1, 1).float()
            h_loc = g.ndata['loc'].type(torch.FloatTensor)
            h_type = g.ndata['type'].type(torch.FloatTensor)
            h = torch.cat((h_degree, h_loc, h_type), axis=-1).cuda()
            h = self.layer1(g, h)
            h = F.elu(h)
            h = self.layer2(g, h)
            g.ndata['h'] = h
            h = dgl.mean_nodes(g, 'h')
            h_list.append(h.unsqueeze(0))
        h = torch.cat(h_list, axis=0)
        h = h.unsqueeze(0)
        h, self.hidden = self.lstm(h, self.hidden)
        h = h.contiguous()
        h = h.view(-1, h.shape[2])
        h = self.hidden_to_tag(h)
        h = h.mean(axis=0)
        h = h.unsqueeze(0)
        return h

default_model = GAT_LSTM