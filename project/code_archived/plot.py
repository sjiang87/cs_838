#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 13:53:56 2019

@author: sjiang87
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
huh = 'BGAT'
lr_dir = r'../results/LSTM/run-.-tag-data_learning_rate.csv'
loss_dir = r'../results/LSTM/run-.-tag-data_training_loss.csv'
t1train_dir = r'../results/{}/run-data_top1_accuracy_train-tag-data_top1_accuracy.csv'.format(huh)
t1val_dir = r'../results/{}/run-data_top1_accuracy_val-tag-data_top1_accuracy.csv'.format(huh)
t3train_dir = r'../results/{}/run-data_top3_accuracy_train-tag-data_top3_accuracy.csv'.format(huh)
t3val_dir = r'../results/{}/run-data_top3_accuracy_val-tag-data_top3_accuracy.csv'.format(huh)
#%%
# plot top1 train and val
plt.figure(figsize=(4,4))
df_train = pd.read_csv(t1train_dir, usecols=['Step','Value']).tail(800)
plt.plot(df_train['Step'], df_train['Value'], label='top-1 train')

df_val = pd.read_csv(t1val_dir, usecols=['Step','Value'])
plt.plot(df_val['Step'], df_val['Value'], label='top-1 val')

plt.legend()
plt.grid()
plt.xlabel('epochs')
plt.ylabel('Accuracy')
plt.tight_layout()
plt.savefig('../results/{}/top1_{}_800.svg'.format(huh, huh))
#%%
# plot top3 train and val
plt.figure(figsize=(4,4))
df_train = pd.read_csv(t3train_dir, usecols=['Step','Value']).tail(800)
plt.plot(df_train['Step'], df_train['Value'], label='top-3 train')

df_val = pd.read_csv(t3val_dir, usecols=['Step','Value'])
plt.plot(df_val['Step'], df_val['Value'], label='top-3 val')
plt.legend()
plt.grid()
plt.xlabel('epochs')
plt.ylabel('Accuracy')
plt.tight_layout()
plt.savefig('../results/{}/top3_{}_800.svg'.format(huh, huh))
#%%
# plot learning rate
plt.figure(figsize=(4,4))
df_lr = pd.read_csv(lr_dir, usecols=['Step','Value'])
plt.plot(df_lr['Step'], df_lr['Value'], label='learning rate')
plt.legend()
plt.grid()
plt.xlabel('epochs')
plt.ylabel('Learning rate')
plt.tight_layout()
plt.savefig('../results/GAT/lr_GAT_800.svg')
#%%
# plot training loss
plt.figure(figsize=(4,4))
df_loss = pd.read_csv(loss_dir, usecols=['Step','Value'])
plt.plot(df_loss['Step'], df_loss['Value'], label='training loss')
plt.legend()
plt.grid()
plt.xlabel('epochs')
plt.ylabel('Training loss')
plt.tight_layout()
plt.savefig('../results/GAT/loss_GAT_800.svg')