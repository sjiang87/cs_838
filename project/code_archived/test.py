#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 21:24:58 2019

@author: sjiang87
"""

# python imports
import glob
import os
import numpy as np

# our code 
from functions import xyz
# directories
data_dir = '../data/*/*/*/*'
files = sorted(glob.glob(os.path.join(data_dir, '*.txt')))
file = np.loadtxt(files[0], delimiter=',')
file = file[:,1:]
for i in range(30):
    location = file[0, i*3:(i+1)*3]
    location = xyz(location)


#%%
import matplotlib.pyplot as plt
import networkx as nx
from sk_graph import SkDataset
dataset = SkDataset()
graph, label = dataset[100]
fig, ax = plt.subplots()
nx.draw(graph.to_networkx(), ax=ax)
ax.set_title('Class: {:s}'.format(label))
plt.show()

#%%
from model_library import default_model
from sk_graph import SkDataset, SkValDataset
import torch
from torchviz import make_dot
from functions import collate
train_dataset = SkDataset()
val_dataset = SkValDataset()

train_loader = torch.utils.data.DataLoader(
    train_dataset, batch_size=400, shuffle=True,
    num_workers=4, pin_memory=True, sampler=None, 
    drop_last=True, collate_fn=collate)
val_loader = torch.utils.data.DataLoader(
    val_dataset, batch_size=100, shuffle=False,
    num_workers=4, pin_memory=True, sampler=None, 
    drop_last=False, collate_fn=collate)
model = default_model(13, 24)
for i, (input, target) in enumerate(train_loader):
    target = target.squeeze()
    target.cuda(0, non_blocking=False)
    model.cuda(0)
    output = model(input)
    make_dot(output)
    break
#%%


import dgl.function as fn
import torch
import torch.nn as nn

#%%

# Sends a message of node feature h.
msg = fn.copy_src(src='h', out='m')

def reduce(nodes):
    """Take an average over all neighbor node features hu and use it to
    overwrite the original node feature."""
    accum = torch.mean(nodes.mailbox['m'], 1)
    return {'h': accum}

class NodeApplyModule(nn.Module):
    """Update the node feature hv with ReLU(Whv+b)."""
    def __init__(self, in_feats, out_feats, activation):
        super(NodeApplyModule, self).__init__()
        self.linear = nn.Linear(in_feats, out_feats)
        self.activation = activation

    def forward(self, node):
        h = self.linear(node.data['h'])
        h = self.activation(h)
        return {'h' : h}

class GCN(nn.Module):
    def __init__(self, in_feats, out_feats, activation):
        super(GCN, self).__init__()
        self.apply_mod = NodeApplyModule(in_feats, out_feats, activation)

    def forward(self, g, feature):
        # Initialize the node features with h.
        g.ndata['h'] = feature
        g.update_all(msg, reduce)
        g.apply_nodes(func=self.apply_mod)
        return g.ndata.pop('h')
    
#%%
import torch.nn.functional as F


class Classifier(nn.Module):
    def __init__(self, in_dim, hidden_dim):
        super(Classifier, self).__init__()

        self.layers = nn.ModuleList([
            GCN(in_dim, hidden_dim, F.relu),
            GCN(hidden_dim, hidden_dim, F.relu),
            GCN(hidden_dim, hidden_dim, F.relu)])
        self.classify = nn.Linear(hidden_dim, 1)

    def forward(self, g):
        # For undirected graphs, in_degree is the same as
        # out_degree.
        h_degree = g.in_degrees().view(-1, 1).float()
        h_pos = g.ndata['h']
        h = torch.cat((h_degree, h_pos), axis=-1).cuda()
        for conv in self.layers:
            h = conv(g, h)
        g.ndata['h'] = h
        hg = dgl.mean_nodes(g, 'h')
        return self.classify(hg)
#%%
import torch.optim as optim
from torch.utils.data import DataLoader
import dgl
import torch
def collate(samples):
    # The input `samples` is a list of pairs
    #  (graph, label).
    graphs, labels = map(list, zip(*samples))
    batched_graph = dgl.batch(graphs)
    return batched_graph, torch.tensor(labels).unsqueeze(-1)
# Create training and test sets.
trainset = HydroDataset()
#testset = MiniGCDataset(80, 10, 20)
# Use PyTorch's DataLoader and the collate function
# defined before.
data_loader = DataLoader(trainset, batch_size=100, shuffle=True,
                         collate_fn=collate)

# Create model
model = Classifier(4, 256).cuda()
loss_func = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=0.001)
model.train()

epoch_losses = []
for epoch in range(200):
    epoch_loss = 0
    for iter, (bg, label) in enumerate(data_loader):
        prediction = model(bg)
        loss = loss_func(prediction, label.cuda())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        epoch_loss += loss.detach().item()
    epoch_loss /= (iter + 1)
    print('Epoch {}, loss {:.4f}'.format(epoch, epoch_loss))
    epoch_losses.append(epoch_loss)
    
#%%
#%%
from model_library import HydroNet
model = HydroNet(4, 64)
pytorch_total_params = sum(p.numel() for p in model.parameters())
print('Total number of parameters: {}'.format(pytorch_total_params))
#%%
from dgl.data import MiniGCDataset
import matplotlib.pyplot as plt
import networkx as nx
# A dataset with 80 samples, each graph is
# of size [10, 20]
dataset = MiniGCDataset(80, 10, 20)
graph, label = dataset[0]
fig, ax = plt.subplots()
nx.draw(graph.to_networkx(), ax=ax)
ax.set_title('Class: {:d}'.format(label))
plt.show()
#%%
trainset = MiniGCDataset(320, 10, 20)
trainset.num_classes
#%%
import matplotlib.pyplot as plt
import numpy as np
x = np.arange(0,1, 1, 0.1)
y = [18, 18, 18, 18, 18, 18, 18, 18, 18]

#%%
import csv 

import re

file5='/content/drive/My Drive/company.csv'

file6='/content/drive/My Drive/firm_name1.csv'

company=open(file5)

company1=csv.reader(company)

company_list=[]

for row in company1:

  ticker=open(file6)

  ticker1=csv.reader(ticker)

  ticker_row=row

  found_firm_flag=0

  for line in ticker1:

    name=line[2].lower() 

    tic=row[0].lower().replace("the ","").replace("."," ").replace(","," ").replace("-"," ").replace("\'","").replace("  "," ")     

    if re.findall(name[0:11],tic[0:11]):

      ticker_row.append(line[1])

      found_firm_flag=1

  if found_firm_flag==0:

    ticker_row.append("")

  company_list.append(ticker_row)

print(company_list)

writer=csv.writer(open('ticker.csv','w', newline=''))

writer.writerows(company_list)

#%%
from __future__ import division
import re
import csv 
              
filepath_list = []
company_names = []
date =[]
total_counts=[]
positive_counts=[]
negative_counts=[]
positive_tone=[]
negative_tone=[]
na_counts=[]

import glob

#OUTPUT file name, firm name, date--Q1 and Q2;
for filename in glob.glob("*.txt"):                  #find all .txt files; 
    filepath_list.append(''.join(filename))                 #''.join() get rid of the quotations in the list;
#print filepath_list
#Q2
for item in filepath_list:
    firmname= re.findall('[0-9\-]*(.*)\_', item)         ##Find all firm name from file name
    company_names.append(''.join(firmname))
    datefirm = re.findall('(.*-[0-9]*).*', item)         ##Find all date from file name
    date.append(''.join(datefirm))

#word lists;
positive = open('positive_fin.csv')
positive_list = positive.read().split()
   
negative = open('negative_fin.csv')
negative_list = negative.read().split()
    
stop = open('stop.csv')
stop_list = stop.read().split()

#Tokenize the files;
for secfile in glob.glob('*.txt'):
    file_content = open(secfile)
    file_list = file_content.read()
    file_process = file_list.upper() #convert all string into upper case to match with the word lists format;
    
    pos_counter=0
    neg_counter=0
    stop_counter=0
    na_counter = 0
    
    #Remove punctuation and white spaces;
    from string import punctuation
    for p in ["\n", "\t", "\a", "\b", "\r", "\v"]:
        file_process = file_process.replace(p,' ')
            
    for p in list(punctuation):
        file_process = file_process.replace(p,'').replace('  ',' ').replace('   ',' ')

        word_each = file_process.split()
    word_counter=len(word_each)
    
    for each in word_each:
        if each == "" or each == " ":
            na_counter = na_counter + 1
                      
        elif each in positive_list:
            pos_counter=pos_counter+1
            
        elif each in negative_list:
            neg_counter=neg_counter+1
            #print each
        elif each in stop_list:
            stop_counter=stop_counter+1
    na_counts.append(na_counter)   
    exstop_counter = word_counter-stop_counter-na_counter  
    positive_counts.append(pos_counter)  
    negative_counts.append(neg_counter)
    total_counts.append(exstop_counter)
    positive_tone.append(pos_counter/exstop_counter) 
    negative_tone.append(neg_counter/exstop_counter)

positive.close()
negative.close()
stop.close()
print na_counts