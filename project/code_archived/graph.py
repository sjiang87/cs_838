#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 10:57:52 2019
This is a dataset for Hydrophobicity data.

@author: Shengli Jiang
"""
from __future__ import absolute_import

import numpy as np

import os, glob
from dgl import DGLGraph
import torch

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder

# file names
data_dir = '../data/*/*/*/*'
files = sorted(glob.glob(os.path.join(data_dir, '*.txt')))

# train_val split
X = [i for i in range(len(files))]
X_train, X_test, _, _ = train_test_split(X, X, test_size=0.2, random_state=0)

# skeleton connects
# we have two people in one frame, so we have node_out and node_out_2
node_out = [1,1,2,2,2,2,2,3,3,3,3,3,3,4,4,4,4,
            5,5,5,6,6,7,7,7,7,8,8,8,10,10,10,
            11,11,11,12,12,13,13,13,14,14,14,
            15,15]
node_out = [i-1 for i in node_out] # because dgl graph node starts from 0
node_out_2 = [i+15 for i in node_out] # shift to the second person
node_in = [1,2,1,2,3,4,7,2,3,4,7,10,13,2,3,4,5,
           4,5,6,5,6,2,3,7,8,7,8,9,3,10,11,
           10,11,12,11,12,3,13,14,13,14,15,
           14,15]
node_in = [i-1 for i in node_in]
node_in_2 = [i+15 for i in node_in]

# node type
# 1 is head, 2 is neck, 3 is torso, 4 is shoulder, 5 is elbow
# 6 is hand, 7 is hip, 8 is knee, 9 is foot
# change it to one hot encoded form
enc = OneHotEncoder(handle_unknown='ignore')
node_type = np.array([1,2,3,4,5,6,4,5,6,7,8,9,7,8,9]).reshape(-1,1)
enc.fit(node_type)
node_type = enc.transform(node_type).toarray()
node_type = np.concatenate((node_type, node_type), axis=0)


class SkDataset(object):
    r"""Mimics citation graph dataset 
    https://docs.dgl.ai/en/latest/_modules/dgl/data/citation_graph.html#CoraDataset
    """
    def __init__(self): 
        self.dir = '../data'
        self.graphs = []
        self.labels = []
        self._load()
        
        
    def _load(self):
        train_file_dir = [files[i] for i in X_train]
        for i in range(len(train_file_dir)):
            file_dir = train_file_dir[i]
            label = int(file_dir.split('/')[4])
            label = np.array([[label]])-1
            with open(file_dir, 'rb') as f:
                data = np.loadtxt(f, delimiter=',')[:,1:]
                video_graph = []
                for j in range(17):
                    if j < data.shape[0]:
                        g = DGLGraph()
                        g.add_nodes(30)
                        g.add_edges(node_out, node_in)
                        g.add_edges(node_out_2, node_in_2)
                        loc = data[j, :].reshape(30, 3)
                        g.ndata['loc'] = torch.from_numpy(loc)
                        g.ndata['type'] = torch.from_numpy(node_type)
                        video_graph.append(g)
                    else:
                        g = DGLGraph()
                        g.add_nodes(30)
                        g.add_edges(node_out, node_in)
                        g.add_edges(node_out_2, node_in_2)
                        loc = data[-1, :].reshape(30, 3)
                        g.ndata['loc'] = torch.from_numpy(loc)
                        g.ndata['type'] = torch.from_numpy(node_type)
                        video_graph.append(g)
                self.graphs.append(video_graph)
                self.labels.append(label)
    def __getitem__(self, idx):
        return self.graphs[idx], self.labels[idx]
    
    def __len__(self):
        return len(self.graphs)
    
class SkValDataset(object):
    def __init__(self): 
        self.dir = '../data'
        self.graphs = []
        self.labels = []
        self._load()

    def _load(self):
        train_file_dir = [files[i] for i in X_test]
        for i in range(len(train_file_dir)):
            file_dir = train_file_dir[i]
            label = int(file_dir.split('/')[4])
            label = np.array([[label]])-1
            with open(file_dir, 'rb') as f:
                data = np.loadtxt(f, delimiter=',')[:,1:]
                video_graph = []
                for j in range(17):
                    if j < data.shape[0]:
                        g = DGLGraph()
                        g.add_nodes(30)
                        g.add_edges(node_out, node_in)
                        g.add_edges(node_out_2, node_in_2)
                        loc = data[j, :].reshape(30, 3)
                        g.ndata['loc'] = torch.from_numpy(loc)
                        g.ndata['type'] = torch.from_numpy(node_type)
                        video_graph.append(g)
                    else:
                        g = DGLGraph()
                        g.add_nodes(30)
                        g.add_edges(node_out, node_in)
                        g.add_edges(node_out_2, node_in_2)
                        loc = data[-1, :].reshape(30, 3)
                        g.ndata['loc'] = torch.from_numpy(loc)
                        g.ndata['type'] = torch.from_numpy(node_type)
                        video_graph.append(g)
                self.graphs.append(video_graph)
                self.labels.append(label)
    
    def __getitem__(self, idx):
        return self.graphs[idx], self.labels[idx]
    
    def __len__(self):
        return len(self.graphs)
    
default_dataset = SkValDataset()

size = []
for i in range(len(default_dataset)):
    size.append(len(default_dataset[i][0]))
                    
                        
                        
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                