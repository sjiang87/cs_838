#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 10:57:52 2019
utils for function

@author: Shengli Jiang
"""
from __future__ import absolute_import

import dgl
import torch

class AverageMeter(object):
  """Computes and stores the average and current value"""
  def __init__(self):
    self.reset()

  def reset(self):
    self.val = 0.0
    self.avg = 0.0
    self.sum = 0
    self.count = 0.0

  def update(self, val, n=1):
    self.val = val
    self.sum += val * n
    self.count += n
    self.avg = self.sum / self.count
    
def collate(samples):
    # The input `samples` is a list of pairs
    #  (graph, label).
    graphs, labels = map(list, zip(*samples))
    batched_graph = dgl.batch(graphs)
    return batched_graph, torch.tensor(labels)

def collate2(samples):
    # The input `samples` is a list of pairs
    #  (graph, label).
    graphs, labels = map(list, zip(*samples))
    return graphs[0], torch.tensor(labels[0][0])

def xyz(location):
    # A function to return normlized location
    # to original location
    location[0] = 1280 - (location[0]*2560)
    location[1] = 960 - (location[1]*1920)
    location[2] = location[2]*10000 / 7.8125
    return location
