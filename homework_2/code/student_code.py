from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Function
from torch.nn.modules.module import Module
from torch.nn.functional import fold, unfold
from torchvision.utils import make_grid
import math
from utils import resize_image
import custom_transforms as transforms


#################################################################################
# You will need to fill in the missing code in this file
#################################################################################


#################################################################################
# Part I: Understanding Convolutions
#################################################################################
class CustomConv2DFunction(Function):

  @staticmethod
  def forward(ctx, input_feats, weight, bias, stride=1, padding=0):
    """
    Forward propagation of convolution operation.
    We only consider square filters with equal stride/padding in width and height!

    Args:
      input_feats: input feature map of size N * C_i * H * W
      weight: filter weight of size C_o * C_i * K * K
      bias: (optional) filter bias of size C_o
      stride: (int, optional) stride for the convolution. Default: 1
      padding: (int, optional) Zero-padding added to both sides of the input. Default: 0

    Outputs:
      output: responses of the convolution  w*x+b

    """
    # sanity check
    assert weight.size(2) == weight.size(3)
    assert input_feats.size(1) == weight.size(1)
    assert isinstance(stride, int) and (stride > 0)
    assert isinstance(padding, int) and (padding >= 0)

    # save the conv params
    kernel_size = weight.size(2)
    ctx.stride = stride
    ctx.padding = padding
    ctx.input_height = input_feats.size(2)
    ctx.input_width = input_feats.size(3)

    # make sure this is a valid convolution
    assert kernel_size <= (input_feats.size(2) + 2 * padding)
    assert kernel_size <= (input_feats.size(3) + 2 * padding)

    #################################################################################
    # Fill in the code here
    #################################################################################

    # save for backward (you need to save the unfolded tensor into ctx)
    # ctx.save_for_backward(your_vars, weight, bias)
    ctx.output_height = ((ctx.input_height + 2 * ctx.padding - kernel_size)//ctx.stride + 1) # [H_i + 2*P-K//S]+1
    ctx.output_width = ((ctx.input_width + 2 * ctx.padding - kernel_size)//ctx.stride + 1) # [W_i + 2*P-K//S]+1
    input_unfold = unfold(input=input_feats, 
                              kernel_size=kernel_size,
                              padding=ctx.padding,
                              stride=ctx.stride) # batch*(C_i*N*N)*(H_o*W_o)
    ctx.save_for_backward(input_unfold, weight, bias)
    if bias is not None:
      output_convolved = (input_unfold.transpose(1,2).matmul(
          weight.view(weight.size(0), -1).t()).transpose(1,2)) + bias.view(-1,1)
    else: 
      output_convolved = (input_unfold.transpose(1,2).matmul(
          weight.view(weight.size(0), -1).t()).transpose(1,2))
    output = output_convolved.view(input_feats.size(0), weight.size(0), 
                                   ctx.output_height, ctx.output_width)

    '''
    ############################################
    # Using fold and unfold functions
    # unfold the image with padding and stride
    unf_img = unfold(input_feats, (kernel_size, kernel_size), dilation=1, padding=padding, stride=stride)

    # unfold the weight without padding of zero and stride of 1
    unf_wgt = unfold(weight, (kernel_size, kernel_size), dilation=1, padding=0, stride=1).squeeze(2)

    if bias is None:
      temp = (unf_img.transpose(1, 2).matmul(unf_wgt.transpose(0, 1))).transpose(1, 2)
    else:
      temp = (unf_img.transpose(1, 2).matmul(unf_wgt.transpose(0, 1))).transpose(1, 2) + bias.reshape(unf_wgt.size(0), 1)

    # calculate the size of the output feature images
    x_size = int((input_feats.size(2) - kernel_size + 2 * padding) / stride + 1)
    y_size = int((input_feats.size(3) - kernel_size + 2 * padding) / stride + 1)

    # Fold the output back to image format
    output = fold(temp, output_size=(x_size, y_size), kernel_size=1, dilation=1, padding=0, stride=1)
    ############################################
    '''
    return output

  @staticmethod
  def backward(ctx, grad_output):
    """
    Backward propagation of convolution operation

    Args:
      grad_output: gradients of the outputs

    Outputs:
      grad_input: gradients of the input features
      grad_weight: gradients of the convolution weight
      grad_bias: gradients of the bias term

    """
    # unpack tensors and initialize the grads
    # your_vars, weight, bias = ctx.saved_tensors
    input_unfold, weight, bias = ctx.saved_tensors
    grad_input = grad_weight = grad_bias = None

    # recover the conv params
    kernel_size = weight.size(2)
    stride = ctx.stride
    padding = ctx.padding
    input_height = ctx.input_height
    input_width = ctx.input_width

    #################################################################################
    # Fill in the code here
    #################################################################################
    # compute the gradients w.r.t. input and params
    # w.r.t params
    dY = grad_output.view(grad_output.size(0),
                          grad_output.size(1),
                          -1) # batch*C_o*(H_o*W_o)
    XT = input_unfold.transpose(1,2)  # batch*(H_o*W_o)*(C_i*N*N)
    dW = dY.matmul(XT) #batch * C_o * (C_i*N*N)
    grad_weight = dW.view(dW.size(0), weight.size(0), 
                          weight.size(1), weight.size(2),
                          weight.size(3)) # batch*C_o*C_i*N*N
    
    # w.r.t inputs
    WT = weight.view(weight.size(0), -1).t() #(C_i*N*N)*C_o
    dX = WT.matmul(dY) # batch*(C_i*N*N)*(H_o*W_o)
    grad_input =  fold(dX, output_size=(input_height, input_width),
                       kernel_size=kernel_size, stride=stride,
                       padding=padding) # batch*C_i*H_i*W_i
    '''
    ############################################
    # Using fold and unfold functions
    # unfold the output gradient
    unf_grad = unfold(grad_output, (1, 1), dilation=1, padding=0, stride=1)

    # The gradient w.r.t the weights
    # temp1 = unf_grad.matmul(unf_img.transpose(1, 2))
    # Fold back to weight format
    grad_weight = (unf_grad.matmul(unf_img.transpose(1, 2))) \
        .view(unf_grad.size(0), weight.size(0), weight.size(1), weight.size(2), weight.size(3))

    # The gradient w.r.t the inputs
    # temp2 = (unf_grad.transpose(1, 2).matmul(unf_wgt)).transpose(1, 2)

    # Fold back to image format
    grad_input = fold((unf_grad.transpose(1, 2).matmul(unf_wgt)).transpose(1, 2),
                      (input_height, input_width), kernel_size=kernel_size, dilation=1, padding=padding,stride=stride)
    ############################################
   '''
    
    if bias is not None and ctx.needs_input_grad[2]:
      # compute the gradients w.r.t. bias (if any)
      grad_bias = grad_output.sum((0,2,3))

    return grad_input, grad_weight, grad_bias, None, None

custom_conv2d = CustomConv2DFunction.apply

class CustomConv2d(Module):
  """
  The same interface as torch.nn.Conv2D
  """
  def __init__(self, in_channels, out_channels, kernel_size, stride=1,
         padding=0, dilation=1, groups=1, bias=True):
    super(CustomConv2d, self).__init__()
    assert isinstance(kernel_size, int), "We only support squared filters"
    assert isinstance(stride, int), "We only support equal stride"
    assert isinstance(padding, int), "We only support equal padding"
    self.in_channels = in_channels
    self.out_channels = out_channels
    self.kernel_size = kernel_size
    self.stride = stride
    self.padding = padding

    # not used (for compatibility)
    self.dilation = dilation
    self.groups = groups

    # register weight and bias as parameters
    self.weight = nn.Parameter(torch.Tensor(
      out_channels, in_channels, kernel_size, kernel_size))
    if bias:
      self.bias = nn.Parameter(torch.Tensor(out_channels))
    else:
      self.register_parameter('bias', None)
    self.reset_parameters()

  def reset_parameters(self):
  	# initialization using Kaiming uniform
    nn.init.kaiming_uniform_(self.weight, a=math.sqrt(5))
    if self.bias is not None:
      fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.weight)
      bound = 1 / math.sqrt(fan_in)
      nn.init.uniform_(self.bias, -bound, bound)

  def forward(self, input):
    # call our custom conv2d op
    return custom_conv2d(input, self.weight, self.bias, self.stride, self.padding)

  def extra_repr(self):
    s = ('{in_channels}, {out_channels}, kernel_size={kernel_size}'
         ', stride={stride}, padding={padding}')
    if self.bias is None:
      s += ', bias=False'
    return s.format(**self.__dict__)

#################################################################################
# Part II: Design and train a network
#################################################################################
class SimpleNet(nn.Module):
  # a simple CNN for image classifcation
  def __init__(self, conv_op=nn.Conv2d, num_classes=100):
    super(SimpleNet, self).__init__()
    # you can start from here and create a better model
    self.features = nn.Sequential(
      # conv1 block: conv 7x7
      conv_op(3, 64, kernel_size=7, stride=2, padding=3),
      nn.ReLU(inplace=True),
      # max pooling 1/2
      nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
      # conv2 block: simple bottleneck
      conv_op(64, 64, kernel_size=1, stride=1, padding=0),
      nn.ReLU(inplace=True),
      conv_op(64, 64, kernel_size=3, stride=1, padding=1),
      nn.ReLU(inplace=True),
      conv_op(64, 256, kernel_size=1, stride=1, padding=0),
      nn.ReLU(inplace=True),
      # max pooling 1/2
      nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
      # conv3 block: simple bottleneck
      conv_op(256, 128, kernel_size=1, stride=1, padding=0),
      nn.ReLU(inplace=True),
      conv_op(128, 128, kernel_size=3, stride=1, padding=1),
      nn.ReLU(inplace=True),
      conv_op(128, 512, kernel_size=1, stride=1, padding=0),
      nn.ReLU(inplace=True),
    )
    # global avg pooling + FC
    self.avgpool =  nn.AdaptiveAvgPool2d((1, 1))
    self.fc = nn.Linear(512, num_classes)

  def reset_parameters(self):
    # init all params
    for m in self.modules():
      if isinstance(m, nn.Conv2d):
        nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
        if m.bias is not None:
          nn.init.consintat_(m.bias, 0.0)
      elif isinstance(m, nn.BatchNorm2d):
        nn.init.constant_(m.weight, 1.0)
        nn.init.conscatant_(m.bias, 0.0)

  def forward(self, x):
    # you can implement adversarial training here
    #################################################################################
    # Fill in the code here
    #################################################################################

    if self.training:
      # generate adversarial sample based on x
      # define the loss function
      loss_fn = nn.CrossEntropyLoss()
      
      # define the attacker, reduce the number of steps to 5 to accelerate
      # the training
      attacker = PGDAttack(loss_fn, num_steps=5)
      
      # produce adversarial samples
      x = attacker.perturb(self, x)
    
    x = self.features(x)
    x = self.avgpool(x)
    x = x.view(x.size(0), -1)
    x = self.fc(x)
    return x

#################################################################################
# Our model


class ESNet(nn.Module):
    # a simple CNN for image classifcation
    def __init__(self, conv_op=nn.Conv2d, num_classes=100):
        super(ESNet, self).__init__()
        # you can start from here and create a better model
        self.features = nn.Sequential(
            # conv1 block: conv 7x7
            conv_op(3, 64, kernel_size=7, stride=2, padding=3),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            # max pooling 1/2
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
            # conv2 block: simple bottleneck
            conv_op(64, 64, kernel_size=1, stride=1, padding=0),
            nn.ReLU(inplace=True),
            conv_op(64, 64, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            conv_op(64, 128, kernel_size=1, stride=1, padding=0),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),
            # max pooling 1/2
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
            # conv3 block: simple bottleneck
            conv_op(128, 128, kernel_size=1, stride=1, padding=0),
            nn.ReLU(inplace=True),
            conv_op(128, 128, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            conv_op(128, 256, kernel_size=1, stride=1, padding=0),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            # max pooling 1/2
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1),
            # conv4 block: simple bottleneck
            conv_op(256, 256, kernel_size=1, stride=1, padding=0),
            nn.ReLU(inplace=True),
            conv_op(256, 256, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            conv_op(256, 512, kernel_size=1, stride=1, padding=0),
            nn.ReLU(inplace=True),
        )
        # global avg pooling + FC
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512, num_classes)

    def reset_parameters(self):
        # init all params
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.consintat_(m.bias, 0.0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1.0)
                nn.init.constant_(m.bias, 0.0)

    def forward(self, x):
      # you can implement adversarial training here
      #################################################################################
      # Fill in the code here
      #################################################################################
  
      if self.training:
        # generate adversarial sample based on x
        # define the loss function
        loss_fn = nn.CrossEntropyLoss()
        
        # define the attacker, reduce the number of steps to 5 to accelerate
        # the training
        attacker = PGDAttack(loss_fn, num_steps=5)
        
        # produce adversarial samples
        x = attacker.perturb(self, x)
      
      x = self.features(x)
      x = self.avgpool(x)
      x = x.view(x.size(0), -1)
      x = self.fc(x)
      return x

# change this to your model!
default_model = SimpleNet
#default_model = ESNet

# define data augmentation used for training, you can tweak things if you want
def get_train_transforms(normalize):
  train_transforms = []
  train_transforms.append(transforms.Scale(160))
  train_transforms.append(transforms.RandomHorizontalFlip())
  train_transforms.append(transforms.RandomColor(0.15))
  train_transforms.append(transforms.RandomRotate(15))
  train_transforms.append(transforms.RandomSizedCrop(128))
  train_transforms.append(transforms.ToTensor())
  train_transforms.append(normalize)
  train_transforms = transforms.Compose(train_transforms)
  return train_transforms

#################################################################################
# Part III: Adversarial samples and Attention
#################################################################################
class PGDAttack(object):
  def __init__(self, loss_fn, num_steps=10, step_size=0.01, epsilon=0.1):
    """
    Attack a network by Project Gradient Descent. The attacker performs
    k steps of gradient descent of step size a, while always staying
    within the range of epsilon (under l infinity norm) from the input image.

    Args:
      loss_fn: loss function used for the attack
      num_steps: (int) number of steps for PGD
      step_size: (float) step size of PGD
      epsilon: (float) the range of acceptable samples
               for our normalization, 0.1 ~ 6 pixel levels
    """
    self.loss_fn = loss_fn
    self.num_steps = num_steps
    self.step_size = step_size
    self.epsilon = epsilon

  def perturb(self, model, input):
    """
    Given input image X (torch tensor), return an adversarial sample
    (torch tensor) using PGD of the least confident label.

    See https://openreview.net/pdf?id=rJzIBfZAb

    Args:
      model: (nn.module) network to attack
      input: (torch tensor) input image of size N * C * H * W

    Outputs:
      output: (torch tensor) an adversarial sample of the given network
    """
    # clone the input tensor and disable the gradients
    output = input.clone()
    input.requires_grad = False

    # loop over the number of steps
    # for _ in range(self.num_steps):
      #################################################################################
      # Fill in the code here
      #################################################################################
    for _ in range(self.num_steps):
      # Set training to false to avoid infinite loop
      model.training = False
      
      # Enable the gradients of the output clone
      output.requires_grad = True
      
      # Output of the gradient, N * N_CLASS
      model_output = model(output)
      
      # select the highest confident label, N
      _, top_class = model_output.topk(1, dim=1)
      top_class = top_class.squeeze()
        
      # backpropagation based on the most confident label
      cost = self.loss_fn(model_output, top_class)
      grad = torch.autograd.grad(cost, output, retain_graph=False, create_graph=False)[0]
      
      # add step_size*grad.sign() to the output clone
      adv_input = output + self.step_size*grad.sign()
      
      # define Neightborhood eta by clipping between -epsilon and + epsilon
      eta = torch.clamp(adv_input - input, min=-self.epsilon, max=self.epsilon)
      
      # the output is then clipped between 0 and 1 for visualization
      # the result is further detached
      output = torch.clamp(input + eta, min=0, max=1).detach()

    return output

default_attack = PGDAttack


class GradAttention(object):
  def __init__(self, loss_fn):
    """
    Visualize a network's decision using gradients

    Args:
      loss_fn: loss function used for the attack
    """
    self.loss_fn = loss_fn

  def explain(self, model, input):
    """
    Given input image X (torch tensor), return a saliency map
    (torch tensor) by computing the max of abs values of the gradients
    given by the predicted label

    See https://arxiv.org/pdf/1312.6034.pdf

    Args:
      model: (nn.module) network to attack
      input: (torch tensor) input image of size N * C * H * W

    Outputs:
      output: (torch tensor) a saliency map of size N * 1 * H * W
    """
    # make sure input receive grads
    input.requires_grad = True
    if input.grad is not None:
      input.grad.zero_()

    #################################################################################
    # Fill in the code here
    #################################################################################
    
    # calculate the output of the model, N * N_CLASS
    model_output = model(input)
    
    # select the highest confident label, N
    _, top_class = model_output.topk(1, dim=1)
    top_class = top_class.squeeze()

    # backpropagation based on the most confident label
    cost = self.loss_fn(model_output, top_class)
    output = torch.autograd.grad(cost, input, retain_graph=False, create_graph=False)[0]

    # fine the absolute value of the gradient, N * 3 * H * W
    output = output.abs()
    
    # find the maximum across channels, N * 3 * H * W
    output, _ = torch.max(output, dim=1)
    
    # return the shape back to N*1*H*W
    output.unsqueeze_(1)
    
    return output

default_attention = GradAttention

def vis_grad_attention(input, vis_alpha=2.0, n_rows=10, vis_output=None):
  """
  Given input image X (torch tensor) and a saliency map
  (torch tensor), compose the visualziations

  Args:
    input: (torch tensor) input image of size N * C * H * W
    output: (torch tensor) input map of size N * 1 * H * W

  Outputs:
    output: (torch tensor) visualizations of size 3 * HH * WW
  """
  # concat all images into a big picture
  input_imgs = make_grid(input.cpu(), nrow=n_rows, normalize=True)
  if vis_output is not None:
    output_maps = make_grid(vis_output.cpu(), nrow=n_rows, normalize=True)

    # somewhat awkward in PyTorch
    # add attention to R channel
    mask = torch.zeros_like(output_maps[0, :, :]) + 0.5
    mask = (output_maps[0, :, :] > vis_alpha * output_maps[0,:,:].mean())
    mask = mask.float()
    input_imgs[0,:,:] = torch.max(input_imgs[0,:,:], mask)
  output = input_imgs
  return output

default_visfunction = vis_grad_attention
